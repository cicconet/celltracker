//
//  View.h
//  CellTracker
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#import <Cocoa/Cocoa.h>
#import "OCVFloatImage.h"
#import "OCVImageProcessing.h"
#import "Tracker.h"

@interface View : NSView {
    NSMutableArray * imagePaths;
    int nFrames;
    int currentFrame;
    OCVFloatImage * currentImage;
    IBOutlet NSTextField * nFramesTextField;
    IBOutlet NSTextField * currentFrameTextField;
    IBOutlet NSSlider * currentFrameSlider;
    Tracker * tracker;
    IBOutlet NSButton * startButton;
    IBOutlet NSButton * trackButton;
    IBOutlet NSButton * pauseButton;
    IBOutlet NSButton * divideButton;
    IBOutlet NSButton * trackOneFrameButton;
    IBOutlet NSTextView * textView;
    int startRow;
    int startCol;
    BOOL cellInitializationPhase;
    OCVFloatImage * histEqImage;
    OCVFloatImage * morpImage;
    OCVFloatImage * gradImage;
    OCVFloatImage * scratchImage;
    float * kernel;
    BOOL keepTracking;
    int indexOfCellToDivide;
    BOOL settingDivision;
    BOOL thereIsAHighlightedCell;
    point p0;
    point p1;
    int highlightedPointIndex;
    int upperBoundIndexForSettingCellDivision;
    int compactionStartsFrame;
    int compactionEndsFrame;
    int blastCavitStartsFrame;
    
    IBOutlet NSButton * cStartsButton;
    IBOutlet NSButton * cEndsButton;
    IBOutlet NSButton * bcStartsButton;
    IBOutlet NSButton * saveButton;
    
    NSString * savingFileName;
    
    BOOL * cellDidDivide;
}

- (IBAction)saveReport:(id)sender;
- (IBAction)setCompactionStarts:(id)sender;
- (IBAction)setCompactionEnds:(id)sender;
- (IBAction)setBlastCavitStarts:(id)sender;
- (IBAction)loadImages:(id)sender;
- (IBAction)setCurrentFrame:(id)sender;
- (void)updateCurrentImage;
- (void)showBoundariesIfNecessary;
- (IBAction)start:(id)sender;
- (IBAction)track:(id)sender;
- (IBAction)trackOneFrame:(id)sender;
- (IBAction)divide:(id)sender;
- (void)setCellToDivide;
- (void)setNewCells;
- (void)switchHighlightedPoint;
- (void)trackMethod:(id)sender;
- (IBAction)pause:(id)sender;
- (void)moveInitPointAtDirection:(NSString *)theDirection nPixels:(int)theNPixels;

@end

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Michelle Gutwein, Kristin C. Gunsalus, and Davi Geiger.
//  Label free cell-tracking and division detection based on 2D time-lapse images for lineage analysis of early embryo development.
//  Computers in Biology and Medicine. Available online 9 May 2014.
//  http://www.sciencedirect.com/science/article/pii/S0010482514000973
