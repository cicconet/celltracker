//
//  OCVFloatImage.m
//  CellTracker
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#import "OCVFloatImage.h"

@implementation OCVFloatImage

@synthesize width, height, data, cgImageRef;

// ----------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Initialization
// ----------------------------------------------------------------------------------------------------

- (id)initWithImageInFilePath:(NSString *)theFilePath
{
    if (self = [super init]) {
        NSURL * url = [NSURL fileURLWithPath:theFilePath];

        CGImageSourceRef image_source = CGImageSourceCreateWithURL((CFURLRef)url, NULL);
        CGImageRef image = CGImageSourceCreateImageAtIndex(image_source, 0, NULL);
        CFRelease(image_source);
        
        width = (int)CGImageGetWidth(image);
        height = (int)CGImageGetHeight(image);
        
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
        unsigned char * rawData = (unsigned char *)malloc(width*height*sizeof(unsigned char));
        NSUInteger bytesPerPixel = 1;
        NSUInteger bytesPerRow = bytesPerPixel*width;
        NSUInteger bitsPerComponent = 8;
        CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                     bitsPerComponent, bytesPerRow, colorSpace,
                                                     kCGBitmapByteOrderDefault);
        CGColorSpaceRelease(colorSpace);
        
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
        
        data = (float *)calloc(width*height, sizeof(float));
        
        for (int i = 0; i < width*height; i++) {
            data[i] = (float)rawData[i]/255.0;
        }
        
        CGContextRelease(context);
        free(rawData);
    }
    return self;
}

- (id)initWithData:(float *)theData width:(int)theWidth height:(int)theHeight
{
    if (self = [super init]) {
        width = theWidth;
        height = theHeight;
        data = (float *)calloc(width*height, sizeof(float));
        if (theData) {
            memcpy(data, theData, width*height*sizeof(float));
        }
    }
    return self;
}

- (void)copyDataFromImage:(OCVFloatImage *)floatImage
{
    // images should be of same size
    memcpy(data, floatImage.data, width*height*sizeof(float));
}

// ----------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Info
// ----------------------------------------------------------------------------------------------------

- (void)printRange
{
    float min = INFINITY;
    float max = -INFINITY;
    for (int i = 0; i < width*height; i++) {
        float v = data[i];
        if (v < min) {
            min = v;
        }
        if (v > max) {
            max = v;
        }
    }
    printf("min: %f, max: %f\n", min, max);
}

- (void)getRangeOutMin:(float *)theMin outMax:(float *)theMax
{
    float min = INFINITY;
    float max = -INFINITY;
    for (int i = 0; i < width*height; i++) {
        float v = data[i];
        if (v < min) min = v;
        if (v > max) max = v;
    }
    *theMin = min;
    *theMax = max;
}

// ----------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Operations
// ----------------------------------------------------------------------------------------------------

- (void)normalize
{
    float min, max;
    [self getRangeOutMin:&min outMax:&max];
    float range = max-min;
    for (int i = 0; i < width*height; i++) {
        data[i] = (data[i]-min)/range;
    }
}

- (void)square
{
    for (int i = 0; i < width*height; i++) {
        data[i] = data[i]*data[i];
    }
}

- (void)setZero
{
    for (int i = 0; i < width*height; i++) {
        data[i] = 0.0;
    }
}

// ----------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark IO
// ----------------------------------------------------------------------------------------------------

- (vImage_Buffer)vImageBufferStructure
{
    vImage_Buffer bf;
    bf.data = self.data;
    bf.height = self.height;
    bf.width = self.width;
    bf.rowBytes = self.width*sizeof(float);
    return bf;
}

- (void)prepareImageRef
{
    if (cgImageRef) {
        CGImageRelease(cgImageRef);
        CGDataProviderRelease(provider);
        CFRelease(data8);
        free(ucImage);
    }
    ucImage = (unsigned char *)malloc(width*height*sizeof(unsigned char));
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            ucImage[i*width+j] = 255*data[i*width+j];
        }
    }
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceGray();
    data8 = CFDataCreate(NULL, ucImage, width*height);
    provider = CGDataProviderCreateWithCFData(data8);
    cgImageRef = CGImageCreate(width, height, 8, 8, width, colorspace, kCGBitmapByteOrderDefault, provider, NULL, true, kCGRenderingIntentDefault);
    CGColorSpaceRelease(colorspace);
}

- (void)savePNGToFilePath:(NSString *)theFilePath
{
    [self prepareImageRef];
    
    NSURL * urlOut = [NSURL fileURLWithPath:theFilePath];
    CGImageDestinationRef myImageDest = CGImageDestinationCreateWithURL((CFURLRef)urlOut, (CFStringRef)@"public.png", 1, NULL);
    CGImageDestinationAddImage(myImageDest, cgImageRef, NULL);
    CGImageDestinationFinalize(myImageDest);
    CFRelease(myImageDest);
}

// ----------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Drawing
// ----------------------------------------------------------------------------------------------------

- (void)paintDotOfHalfSize:(int)theHalfSize atRow:(int)theRow col:(int)theCol
{
    int row = theRow;
    int col = theCol;
    if (row < theHalfSize) row = theHalfSize;
    if (row > height-(theHalfSize+1)) row = height-(theHalfSize+1);
    if (col < theHalfSize) col = theHalfSize;
    if (col > width-(theHalfSize+1)) col = width-(theHalfSize+1);
    for (int i = -theHalfSize; i < theHalfSize; i++) {
        for (int j = -theHalfSize; j < theHalfSize; j++) {
            int r = row+i;
            int c = col+j;
            data[r*width+c] = 1.0;
        }
    }
}

- (void)paintHollowDotOfHalfSize:(int)theHalfSize atRow:(int)theRow col:(int)theCol highlighted:(BOOL)isHighlighted
{
    int row = theRow;
    int col = theCol;
    if (row < theHalfSize) row = theHalfSize;
    if (row > height-(theHalfSize+1)) row = height-(theHalfSize+1);
    if (col < theHalfSize) col = theHalfSize;
    if (col > width-(theHalfSize+1)) col = width-(theHalfSize+1);
    for (int i = -theHalfSize; i < theHalfSize; i++) {
        int j = -theHalfSize;
        int r = row+i;
        int c = col+j;
        data[r*width+c] = 1.0;
        j = theHalfSize-1;
        r = row+i;
        c = col+j;
        data[r*width+c] = 1.0;
    }
    for (int j = -theHalfSize; j < theHalfSize; j++) {
        int i = -theHalfSize;
        int r = row+i;
        int c = col+j;
        data[r*width+c] = 1.0;
        i = theHalfSize-1;
        r = row+i;
        c = col+j;
        data[r*width+c] = 1.0;
    }
    if (isHighlighted && theHalfSize > 3) {
        [self paintDotOfHalfSize:2 atRow:theRow col:theCol];
    }
}

// ----------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Clean Up
// ----------------------------------------------------------------------------------------------------

-(void)dealloc
{
    if (cgImageRef) {
        CGImageRelease(cgImageRef);
        CGDataProviderRelease(provider);
        CFRelease(data8);
        free(ucImage);
    }
    free(data);
    [super dealloc];
}

@end

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Michelle Gutwein, Kristin C. Gunsalus, and Davi Geiger.
//  Label free cell-tracking and division detection based on 2D time-lapse images for lineage analysis of early embryo development.
//  Computers in Biology and Medicine. Available online 9 May 2014.
//  http://www.sciencedirect.com/science/article/pii/S0010482514000973
