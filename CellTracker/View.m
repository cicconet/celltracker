//
//  View.m
//  CellTracker
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#import "View.h"

@implementation View

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        currentImage = [[OCVFloatImage alloc] initWithData:NULL width:400 height:400];
        kernel = (float *)malloc(25*sizeof(float)); // used for morphological operations (Matlab's strel('ball',3,3))
//        float k[49] = {
//            0.7498,    1.1247,    1.4996,    1.8745,    1.4996,    1.1247,    0.7498,
//            1.1247,    1.4996,    1.8745,    2.2494,    1.8745,    1.4996,    1.1247,
//            1.4996,    1.8745,    2.2494,    2.6243,    2.2494,    1.8745,    1.4996,
//            1.8745,    2.2494,    2.6243,    2.9992,    2.6243,    2.2494,    1.8745,
//            1.4996,    1.8745,    2.2494,    2.6243,    2.2494,    1.8745,    1.4996,
//            1.1247,    1.4996,    1.8745,    2.2494,    1.8745,    1.4996,    1.1247,
//            0.7498,    1.1247,    1.4996,    1.8745,    1.4996,    1.1247,    0.7498
//        };
//        memcpy(kernel, k, 49*sizeof(float));
        for (int i = 0; i < 25; i++) {
            kernel[i] = 1.0;
        }
        [self setNeedsDisplay:YES];
        
        cellDidDivide = (BOOL *)calloc(7, sizeof(BOOL));
    }
    
    return self;
}

- (BOOL)acceptsFirstResponder
{
    return YES;
}

- (void)awakeFromNib
{
    [textView setEditable:NO];
    [textView setSelectable:NO];
    [textView setString:@"Click Open to load frames."];
}

- (IBAction)loadImages:(id)sender
{
    NSOpenPanel * panel = [NSOpenPanel openPanel];
    panel.canChooseFiles = NO;
    panel.canChooseDirectories = YES;
	[panel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result){
        if (result == NSOKButton) {
            NSURL * url = panel.URL;
            
            NSString * embryo = [url lastPathComponent];
            NSString * experiment = [[url URLByDeletingLastPathComponent] lastPathComponent];
            if (savingFileName) {
                [savingFileName release];
            }
            savingFileName = [[NSString alloc] initWithString:[experiment stringByAppendingString:embryo]];
            
            NSArray * array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[url path] error:nil];
            NSArray * images = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.png'"]];
            if ([images count] == 0) {
                images = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.tiff'"]];
            }
            if (imagePaths) {
                [imagePaths release];
                [tracker release];
                [gradImage release];
                [histEqImage release];
                [morpImage release];
                [scratchImage release];
            }
            nFrames = (int)[images count];
            currentFrame = 0;
            imagePaths = [[NSMutableArray alloc] initWithCapacity:nFrames];
            for (NSString * path in images){
                [imagePaths addObject:[[url path] stringByAppendingFormat:@"/%@",path]];
            }
            
            [currentImage release];
            currentImage = [[OCVFloatImage alloc] initWithImageInFilePath:[imagePaths objectAtIndex:currentFrame]];
            [self setNeedsDisplay:YES];
            [currentFrameTextField setStringValue:[NSString stringWithFormat:@"%d", currentFrame]];
            
            gradImage = [[OCVFloatImage alloc] initWithData:NULL width:currentImage.width height:currentImage.height];
            histEqImage = [[OCVFloatImage alloc] initWithData:NULL width:currentImage.width height:currentImage.height];
            morpImage = [[OCVFloatImage alloc] initWithData:NULL width:currentImage.width height:currentImage.height];
            scratchImage = [[OCVFloatImage alloc] initWithData:NULL width:currentImage.width height:currentImage.height];
            tracker = [[Tracker alloc] initForNFrames:nFrames];
            
            [nFramesTextField setStringValue:[NSString stringWithFormat:@"%d", nFrames]];
            [currentFrameSlider setFloatValue:0.0];
            
            [currentFrameSlider setEnabled:YES];
            [startButton setEnabled:YES];
            [trackButton setEnabled:NO];
            [trackOneFrameButton setEnabled:NO];
            [pauseButton setEnabled:NO];
            [divideButton setEnabled:NO];
            
            settingDivision = NO;
            thereIsAHighlightedCell = NO;
            
            for (int frame = currentFrame; frame < nFrames; frame++) {
                for (int i = 0; i < 7; i++) {
                    tracker.cells[i][frame].active = NO;
                    tracker.cells[i][frame].tracked = NO;
                }
            }
            
            upperBoundIndexForSettingCellDivision = -1;
            
            [textView setString:@"Click Start to initialize tracking.\nUse the slider to navigate between frames.\nOptionally, use the right or left arrows to navigate between frames.\nPress SHIFT with the right or left arrows for faster navigation."];
            
            compactionStartsFrame = 0;
            compactionEndsFrame = 0;
            blastCavitStartsFrame = 0;
            
            [cStartsButton setEnabled:YES];
            [cEndsButton setEnabled:YES];
            [bcStartsButton setEnabled:YES];
            [saveButton setEnabled:YES];
        }
    }];
}

- (IBAction)saveReport:(id)sender
{
    NSSavePanel * panel = [NSSavePanel savePanel];
    [panel setNameFieldStringValue:[savingFileName stringByAppendingString:@".txt"]];
    [panel setCanSelectHiddenExtension:NO];
    [panel setExtensionHidden:YES];
	[panel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result){
        if (result == NSOKButton) {
            
            int start[7];
            int end[7];
            
            for (int i = 0; i < 7; i++) {
                start[i] = 0;
                end[i] = 0;
            }
            
            for (int i = 0; i < 7; i++) {
                for (int frame = 0; frame < nFrames; frame++) {
                    if (tracker.cells[i][frame].active) {
                        start[i] = frame;
                        break;
                    }
                }
            }
            for (int i = 0; i < 7; i++) {
                for (int frame = 1; frame < nFrames; frame++) {
                    if (tracker.cells[i][frame-1].active && !tracker.cells[i][frame].active) {
                        end[i] = frame-1;
                        break;
                    }
                }
            }
            
            NSURL * url = panel.URL;
            FILE * f = fopen([[url path] cStringUsingEncoding:NSUTF8StringEncoding], "w");
            
            for (int i = 0; i < 7; i++) {
                if (!cellDidDivide[i]) {
                    end[i] = 0;
                }
                fprintf(f, "%d\t%d\n", start[i], end[i]);
            }
            fprintf(f, "%d\n%d\n%d\n", compactionStartsFrame, compactionEndsFrame, blastCavitStartsFrame);
            
            fprintf(f, "%d\t%d\t%d\n", nFrames, currentImage.width, currentImage.height);
            // centers, radii
            for (int frame = 0; frame < nFrames; frame++) {
                int nNonActiveCells = 0;
                for (int i = 0; i < 7; i++) {
                    if (tracker.cells[i][frame].active) {
                        fprintf(f, "%d\t%d\t%d\t", tracker.cells[i][frame].centerRow, tracker.cells[i][frame].centerCol, tracker.cells[i][frame].radius);
                    } else {
                        nNonActiveCells += 1;
                        fprintf(f, "0\t0\t0\t");
                    }
                }
                fprintf(f, "\n");
                if (nNonActiveCells == 7) {
                    break;
                }
            }
            fclose(f);
        }
    }];
}

- (IBAction)setCompactionStarts:(id)sender
{
    compactionStartsFrame = currentFrame;
}

- (IBAction)setCompactionEnds:(id)sender
{
    compactionEndsFrame = currentFrame;
}

- (IBAction)setBlastCavitStarts:(id)sender
{
    blastCavitStartsFrame = currentFrame;
}

- (IBAction)setCurrentFrame:(id)sender
{
    if ([sender floatValue] == 1.0) {
        currentFrame = nFrames-1;
    } else {
        currentFrame = floorf([sender floatValue]*(float)nFrames);
    }
    
    [self updateCurrentImage];
    [self showBoundariesIfNecessary];
    [self setNeedsDisplay:YES];
    if (currentFrame <= upperBoundIndexForSettingCellDivision) {
        [trackButton setEnabled:YES];
        [trackOneFrameButton setEnabled:YES];
    } else {
        [trackButton setEnabled:NO];
        [trackOneFrameButton setEnabled:NO];
    }
    if ([divideButton isEnabled]) [divideButton setEnabled:NO];
    [currentFrameTextField setStringValue:[NSString stringWithFormat:@"%d", currentFrame]];
    
    thereIsAHighlightedCell = NO;
}

- (BOOL)performKeyEquivalent:(NSEvent *)theEvent
{
    BOOL r = NO;
    if (imagePaths) {
        if ([theEvent keyCode] == 123) { // left key
            if (([theEvent modifierFlags] & NSCommandKeyMask) == NSCommandKeyMask) {
                if (([theEvent modifierFlags] & NSShiftKeyMask) == NSShiftKeyMask) {
                    [self moveInitPointAtDirection:@"left" nPixels:5];
                } else {
                    [self moveInitPointAtDirection:@"left" nPixels:1];
                }
                r = YES;
            } else {
                if (currentFrameSlider.isEnabled) {
                    int d;
                    if (([theEvent modifierFlags] & NSShiftKeyMask) == NSShiftKeyMask) {
                        d = 5;
                    } else {
                        d = 1;
                    }
                    currentFrame -= d;
                    if (currentFrame < 0) {
                        currentFrame = 0;
                    }
                    [self updateCurrentImage];
                    [self showBoundariesIfNecessary];
                    [self setNeedsDisplay:YES];
                    [currentFrameTextField setStringValue:[NSString stringWithFormat:@"%d", currentFrame]];
                    [currentFrameSlider setFloatValue:(float)currentFrame/(float)nFrames];
                    if (currentFrame <= upperBoundIndexForSettingCellDivision) {
                        [trackButton setEnabled:YES];
                        [trackOneFrameButton setEnabled:YES];
                    } else {
                        [trackButton setEnabled:NO];
                        [trackOneFrameButton setEnabled:NO];
                    }
                    if ([divideButton isEnabled]) [divideButton setEnabled:NO];
                    thereIsAHighlightedCell = NO;
                    r = YES;
                }
            }
        } else if ([theEvent keyCode] == 124) { // right key
            if (([theEvent modifierFlags] & NSCommandKeyMask) == NSCommandKeyMask) {
                if (([theEvent modifierFlags] & NSShiftKeyMask) == NSShiftKeyMask) {
                    [self moveInitPointAtDirection:@"right" nPixels:5];
                } else {
                    [self moveInitPointAtDirection:@"right" nPixels:1];
                }
                r = YES;
            } else {
                if (currentFrameSlider.isEnabled) {
                    int d;
                    if (([theEvent modifierFlags] & NSShiftKeyMask) == NSShiftKeyMask) {
                        d = 5;
                    } else {
                        d = 1;
                    }
                    currentFrame += d;
                    if (currentFrame > nFrames-1) {
                        currentFrame = nFrames-1;
                    }
                    [self updateCurrentImage];
                    [self showBoundariesIfNecessary];
                    [self setNeedsDisplay:YES];
                    [currentFrameTextField setStringValue:[NSString stringWithFormat:@"%d", currentFrame]];
                    [currentFrameSlider setFloatValue:(float)currentFrame/(float)nFrames];
                    if (currentFrame <= upperBoundIndexForSettingCellDivision) {
                        [trackButton setEnabled:YES];
                        [trackOneFrameButton setEnabled:YES];
                    } else {
                        [trackButton setEnabled:NO];
                        [trackOneFrameButton setEnabled:NO];
                    }
                    if ([divideButton isEnabled]) [divideButton setEnabled:NO];
                    thereIsAHighlightedCell = NO;
                    r = YES;
                }
            }
        } else if ([theEvent keyCode] == 125) { // down key
            if (([theEvent modifierFlags] & NSCommandKeyMask) == NSCommandKeyMask) {
                if (([theEvent modifierFlags] & NSShiftKeyMask) == NSShiftKeyMask) {
                    [self moveInitPointAtDirection:@"down" nPixels:5];
                } else {
                    [self moveInitPointAtDirection:@"down" nPixels:1];
                }
                r = YES;
            }
        } else if ([theEvent keyCode] == 126) { // up key
            if (([theEvent modifierFlags] & NSCommandKeyMask) == NSCommandKeyMask) {
                if (([theEvent modifierFlags] & NSShiftKeyMask) == NSShiftKeyMask) {
                    [self moveInitPointAtDirection:@"up" nPixels:5];
                } else {
                    [self moveInitPointAtDirection:@"up" nPixels:1];
                }
                r = YES;
            }
        } else if ([theEvent keyCode] == 49) { // space
            if (settingDivision) {
                [self switchHighlightedPoint];
                [textView setString:@"Press SPACE to switch between points.\nUse COMMAND or COMMAND+SHIFT plus the directional arrows to adjust the position of the point.\nPress Enter to conclude."];
            } else {
                [self setCellToDivide];
                [textView setString:@"Press SPACE to switch between cells.\nUse COMMAND or COMMAND+SHIFT plus the directional arrows to adjust the position of the boundary.\nClick Divide to set cell division."];
            }
            r = YES;
        } else if ([theEvent keyCode] == 36) { // enter
            if (settingDivision) {
                [textView setString:@"Click Track or Track One Frame to resume tracking."];
                [self setNewCells];
                settingDivision = NO;
                r = YES;
            }
        }
    }
    return r;
}

- (void)moveInitPointAtDirection:(NSString *)theDirection nPixels:(int)theNPixels
{
    int dr = 0;
    int dc = 0;
    if ([theDirection isEqualToString:@"left"]) {
        startCol -= theNPixels; dc = -theNPixels; dr = 0;
    } else if ([theDirection isEqualToString:@"right"]) {
        startCol += theNPixels; dc = theNPixels; dr = 0;
    } else if ([theDirection isEqualToString:@"down"]) {
        startRow += theNPixels; dr = theNPixels; dc = 0;
    } else if ([theDirection isEqualToString:@"up"]) {
        startRow -= theNPixels; dr = -theNPixels; dc = 0;
    }
    if (startRow < 1) startRow = 1;
    if (startRow > currentImage.height-1) startRow = currentImage.height-1;
    if (startCol < 1) startCol = 1;
    if (startCol > currentImage.width-1) startCol = currentImage.width-1;
    if (cellInitializationPhase) {
        [self updateCurrentImage];
        [currentImage paintDotOfHalfSize:2 atRow:startRow col:startCol];
        [self setNeedsDisplay:YES];
    } else if (settingDivision) {
        point * highlightedPoint;
        point * otherPoint;
        if (highlightedPointIndex == 0) {
            highlightedPoint = &p0;
            otherPoint = &p1;
        } else {
            highlightedPoint = &p1;
            otherPoint = &p0;
        }
        highlightedPoint->row = startRow;
        highlightedPoint->col = startCol;
        [self updateCurrentImage];
        [currentImage paintHollowDotOfHalfSize:4 atRow:highlightedPoint->row col:highlightedPoint->col highlighted:YES];
        [currentImage paintHollowDotOfHalfSize:4 atRow:otherPoint->row col:otherPoint->col highlighted:NO];
        [self setNeedsDisplay:YES];
    } else if (thereIsAHighlightedCell) {
        [self updateCurrentImage];
        for (int i = 0; i < 7; i++) {
            Cell * cell = tracker.cells[i][currentFrame];
            if (i != indexOfCellToDivide) {
                if (cell.active) {
                    for (int j = 0; j < cell.nLocations; j++) {
                        int row = cell.boundary[j].row;
                        int col = cell.boundary[j].col;
                        [currentImage paintDotOfHalfSize:1 atRow:row col:col];
                    }
                }
            } else {
                if (cell.active) {
                    cell.centerRow += dr;
                    cell.centerCol += dc;
                    for (int j = 0; j < cell.nLocations; j++) {
                        cell.boundary[j].row += dr;
                        cell.boundary[j].col += dc;
                        int row = cell.boundary[j].row;
                        int col = cell.boundary[j].col;
                        [currentImage paintDotOfHalfSize:2 atRow:row col:col];
                    }
                }
            }
            
        }
        [self setNeedsDisplay:YES];
    }
}

- (void)updateCurrentImage
{
    OCVFloatImage * image = [[OCVFloatImage alloc] initWithImageInFilePath:[imagePaths objectAtIndex:currentFrame]];
//    OCVFloatImage * image2 = [[OCVFloatImage alloc] initWithData:NULL width:image.width height:image.height];
//    [OCVImageProcessing adaptiveHistogramEqualization:image2 input:image nBlockRows:1 nBlockCols:1];
//    [OCVImageProcessing histogramEqualization:image2 input:image];
    [currentImage copyDataFromImage:image];
//    [image2 release];
    [image release];
}

- (void)showBoundariesIfNecessary
{
    for (int i = 0; i < 7; i++) {
        Cell * cell = tracker.cells[i][currentFrame];
        if (cell.active) {
            for (int j = 0; j < cell.nLocations; j++) {
                int row = cell.boundary[j].row;
                int col = cell.boundary[j].col;
                [currentImage paintDotOfHalfSize:1 atRow:row col:col];
            }
        }
    }
}

- (IBAction)start:(id)sender
{
    currentFrame = 0;
    [self updateCurrentImage];
    startRow = currentImage.height/2;
    startCol = currentImage.width/2;
    [currentImage paintDotOfHalfSize:2 atRow:startRow col:startCol];
    [self setNeedsDisplay:YES];
    [currentFrameTextField setStringValue:[NSString stringWithFormat:@"%d", currentFrame]];
    [currentFrameSlider setFloatValue:0.0];
    [currentFrameSlider setEnabled:NO];
    [startButton setEnabled:NO];
    [trackButton setEnabled:YES];
    [trackOneFrameButton setEnabled:YES];
    cellInitializationPhase = YES;
    [textView setString:@"Press COMMAND or COMMAND+SHIFT plus the directional arrows to change the location of the initial center. COMMAND+SHIFT allows faster movement.\nClick Track or Track One Frame to start tracking."];
}

- (IBAction)track:(id)sender
{
    if (settingDivision) {
        [self setNewCells];
        settingDivision = NO;
    }
    
    NSNumber * nTrackingSteps;
    if (!sender) {
        nTrackingSteps = [NSNumber numberWithInt:2];
        [textView setString:@"Press the SPACE key to set cell division or adjust tracking."];
    } else {
        nTrackingSteps = [NSNumber numberWithInt:INFINITY];
        [textView setString:@"Click Pause to stop tracking."];
    }
    
    keepTracking = YES;
    settingDivision = NO;
    
    for (int i = 0; i < 7; i++) {
        tracker.cells[i][currentFrame].tracked = NO;
    }
    if (currentFrame < nFrames-1) {
        for (int frame = currentFrame+1; frame < nFrames; frame++) {
            for (int i = 0; i < 7; i++) {
                tracker.cells[i][frame].active = NO;
                tracker.cells[i][frame].tracked = NO;
            }
        }
    }
    
    if (currentFrame == 0) {
        Cell * cell = tracker.cells[0][currentFrame];
        cell.active = YES;
        cell.generation = 1;
        cell.centerRow = startRow;
        cell.centerCol = startCol;
        cell.radius = [tracker radiusForGeneration:cell.generation];
    }
    
    [NSThread detachNewThreadSelector:@selector(trackMethod:) toTarget:self withObject:nTrackingSteps];
    
    for (int i = 0; i < 7; i++) {
        if (tracker.cells[i][currentFrame].active) {
            cellDidDivide[i] = NO;
        }
    }
    
    if ([nTrackingSteps intValue] == 2) {
        [currentFrameSlider setEnabled:YES];
    } else {
        [pauseButton setEnabled:YES];
        [trackButton setEnabled:NO];
        [trackOneFrameButton setEnabled:NO];
        [currentFrameSlider setEnabled:NO];
    }
}

- (IBAction)trackOneFrame:(id)sender
{
    [self track:nil];
}

- (void)trackMethod:(id)sender
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    int nTrackingSteps = [sender intValue];
    
    int steps = 0;
    while (keepTracking && steps < nTrackingSteps) {
        upperBoundIndexForSettingCellDivision = currentFrame;
        
        cellInitializationPhase = NO;
        
        OCVFloatImage * image = [[OCVFloatImage alloc] initWithImageInFilePath:[imagePaths objectAtIndex:currentFrame]];
        
        for (int i = 0; i < 2; i++) {
            [OCVImageProcessing erosion:scratchImage input:image kernel:kernel kernelSize:5];
            [OCVImageProcessing erosion:image input:scratchImage kernel:kernel kernelSize:5];
        }
        for (int i = 0; i < 2; i++) {
            [OCVImageProcessing dilatation:scratchImage input:image kernel:kernel kernelSize:5];
            [OCVImageProcessing dilatation:image input:scratchImage kernel:kernel kernelSize:5];
        }
        [OCVImageProcessing gradient:gradImage input:image];
        [image release];
        
        [tracker trackWithGradientImage:gradImage index:currentFrame];
        
        [self updateCurrentImage];
        [self showBoundariesIfNecessary];
        [self setNeedsDisplay:YES];
        
        [currentFrameSlider setFloatValue:(float)currentFrame/(float)nFrames];
        [currentFrameTextField setStringValue:[NSString stringWithFormat:@"%d", currentFrame]];
        
        if (currentFrame < nFrames-1) {
            for (int i = 0; i < 7; i++) {
                Cell * cell = tracker.cells[i][currentFrame];
                Cell * nextCell = tracker.cells[i][currentFrame+1];
                nextCell.active = cell.active;
                nextCell.generation = cell.generation;
                nextCell.centerRow = cell.centerRow;
                nextCell.centerCol = cell.centerCol;
                nextCell.radius = cell.radius;
            }
        } else {
            [pauseButton setEnabled:NO];
            [currentFrameSlider setEnabled:YES];
            [textView setString:@""];
        }
        
        currentFrame += 1;
        if (currentFrame == nFrames) {
            keepTracking = NO;
            [trackOneFrameButton setEnabled:NO];
            [trackButton setEnabled:NO];
        }
        steps += 1;
    }
    
    currentFrame -= 1;
    [self updateCurrentImage];
    [self showBoundariesIfNecessary];
    [self setNeedsDisplay:YES];
    [currentFrameSlider setFloatValue:(float)currentFrame/(float)nFrames];
    [currentFrameTextField setStringValue:[NSString stringWithFormat:@"%d", currentFrame]];
    
    [pool release];
}

- (void)setCellToDivide
{
    if (currentFrame > 0 && currentFrame <= upperBoundIndexForSettingCellDivision) {
        int n = 0;
        for (int i = 0; i < 7; i++) {
            if (tracker.cells[i][currentFrame].tracked) {
                n += 1;
            }
        }
        if (n == 1) {
            for (int i = 0; i < 7; i++) {
                Cell * cell = tracker.cells[i][currentFrame];
                if (cell.tracked) {
                    [self updateCurrentImage];
                    for (int j = 0; j < cell.nLocations; j++) {
                        int row = cell.boundary[j].row;
                        int col = cell.boundary[j].col;
                        [currentImage paintDotOfHalfSize:2 atRow:row col:col];
                    }
                    indexOfCellToDivide = i;
                    break;
                }
            }
        } else {
            int i = indexOfCellToDivide+1;
            while (i < 14) {
                int index = (i % 7);
                Cell * cell = tracker.cells[index][currentFrame];
                if (cell.tracked) {
                    [self updateCurrentImage];
                    for (int j = 0; j < cell.nLocations; j++) {
                        int row = cell.boundary[j].row;
                        int col = cell.boundary[j].col;
                        [currentImage paintDotOfHalfSize:2 atRow:row col:col];
                    }
                    indexOfCellToDivide = index;
                    break;
                }
                i += 1;
            }
            i = indexOfCellToDivide+1;
            int count = 0;
            while (i < 14 && count < n-1) {
                int index = (i % 7);
                Cell * cell = tracker.cells[index][currentFrame];
                if (cell.tracked) {
                    for (int j = 0; j < cell.nLocations; j++) {
                        int row = cell.boundary[j].row;
                        int col = cell.boundary[j].col;
                        [currentImage paintDotOfHalfSize:1 atRow:row col:col];
                    }
                    count += 1;
                }
                i += 1;
            }
        }
        [self setNeedsDisplay:YES];
        [divideButton setEnabled:YES];
        thereIsAHighlightedCell = YES;
    }
}

- (void)setNewCells
{
    [self updateCurrentImage];
    [currentImage paintDotOfHalfSize:2 atRow:p0.row col:p0.col];
    [currentImage paintDotOfHalfSize:2 atRow:p1.row col:p1.col];
    
    switch (indexOfCellToDivide) {
        case 0: {
            Cell * daughterCell = tracker.cells[1][currentFrame];
            daughterCell.active = YES;
            daughterCell.tracked = NO;
            daughterCell.generation = 2;
            daughterCell.centerRow = p0.row;
            daughterCell.centerCol = p0.col;
            daughterCell.radius = daughterCell.generation;
            
            daughterCell = tracker.cells[2][currentFrame];
            daughterCell.active = YES;
            daughterCell.tracked = NO;
            daughterCell.generation = 2;
            daughterCell.centerRow = p1.row;
            daughterCell.centerCol = p1.col;
            daughterCell.radius = daughterCell.generation;
            break;
        }
        case 1: {
            Cell * daughterCell = tracker.cells[3][currentFrame];
            daughterCell.active = YES;
            daughterCell.tracked = NO;
            daughterCell.generation = 3;
            daughterCell.centerRow = p0.row;
            daughterCell.centerCol = p0.col;
            daughterCell.radius = daughterCell.generation;
            
            daughterCell = tracker.cells[4][currentFrame];
            daughterCell.active = YES;
            daughterCell.tracked = NO;
            daughterCell.generation = 3;
            daughterCell.centerRow = p1.row;
            daughterCell.centerCol = p1.col;
            daughterCell.radius = daughterCell.generation;
            break;
        }
        case 2: {
            Cell * daughterCell = tracker.cells[5][currentFrame];
            daughterCell.active = YES;
            daughterCell.tracked = NO;
            daughterCell.generation = 3;
            daughterCell.centerRow = p0.row;
            daughterCell.centerCol = p0.col;
            daughterCell.radius = daughterCell.generation;
            
            daughterCell = tracker.cells[6][currentFrame];
            daughterCell.active = YES;
            daughterCell.tracked = NO;
            daughterCell.generation = 3;
            daughterCell.centerRow = p1.row;
            daughterCell.centerCol = p1.col;
            daughterCell.radius = daughterCell.generation;
            break;
        }
        default:
            break;
    }
    
    [self setNeedsDisplay:YES];
}

- (IBAction)divide:(id)sender
{    
    thereIsAHighlightedCell = NO;
    [currentFrameSlider setEnabled:NO];
    Cell * cell = tracker.cells[indexOfCellToDivide][currentFrame];
    
    cellDidDivide[indexOfCellToDivide] = YES;
    
    cell.active = NO;
    cell.tracked = NO;
    if (currentFrame < nFrames-1) {
        for (int frame = currentFrame+1; frame < nFrames; frame++) {
            Cell * thisCell = tracker.cells[indexOfCellToDivide][frame];
            thisCell.active = NO;
            thisCell.tracked = NO;
        }
    }
    upperBoundIndexForSettingCellDivision = currentFrame;
    
    if (cell.generation < 3) {
        [textView setString:@"Press SPACE to switch between points.\nUse COMMAND or COMMAND+SHIFT plus the directional arrows to adjust the position of the point.\nPress Enter to conclude."];
        
        settingDivision = YES;
        
        int r = [tracker radiusForGeneration:cell.generation+1];
        
        p0.row = cell.centerRow;
        p1.row = cell.centerRow;
        p0.col = cell.centerCol-r/2;
        p1.col = cell.centerCol+r/2;
        
        highlightedPointIndex = 1;
        [self switchHighlightedPoint];
    } else {
        [textView setString:@"Press the SPACE key to set cell division or adjust tracking.\nClick Track or Track One Frame to resume tracking."];
        settingDivision = NO;
        
        [currentFrameSlider setEnabled:YES];
        [self updateCurrentImage];
        for (int i = 0; i < 7; i++) {
            Cell * thisCell = tracker.cells[i][currentFrame];
            if (thisCell.tracked) {
                for (int j = 0; j < thisCell.nLocations; j++) {
                    int row = thisCell.boundary[j].row;
                    int col = thisCell.boundary[j].col;
                    [currentImage paintDotOfHalfSize:1 atRow:row col:col];
                }
            }
        }
        [self setNeedsDisplay:YES];
    }
    [divideButton setEnabled:NO];
}

- (void)switchHighlightedPoint
{
    point highlightedPoint;
    point otherPoint;
    if (highlightedPointIndex == 0) {
        highlightedPointIndex = 1;
        highlightedPoint = p1;
        otherPoint = p0;
    } else {
        highlightedPointIndex = 0;
        highlightedPoint = p0;
        otherPoint = p1;
    }
    startRow = highlightedPoint.row;
    startCol = highlightedPoint.col;
    [self updateCurrentImage];
    [currentImage paintHollowDotOfHalfSize:4 atRow:highlightedPoint.row col:highlightedPoint.col highlighted:YES];
    [currentImage paintHollowDotOfHalfSize:4 atRow:otherPoint.row col:otherPoint.col highlighted:NO];
    [self setNeedsDisplay:YES];
}

- (IBAction)pause:(id)sender
{
    [textView setString:@"Press the SPACE key to set cell division or adjust tracking."];
    keepTracking = NO;
    [trackButton setEnabled:YES];
    [trackOneFrameButton setEnabled:YES];
    [pauseButton setEnabled:NO];
    [currentFrameSlider setEnabled:YES];
}

- (void)drawRect:(NSRect)dirtyRect
{
    if (currentImage) {
        [currentImage prepareImageRef];
        NSImage * image = [[NSImage alloc] initWithCGImage:currentImage.cgImageRef size:NSMakeSize(currentImage.width, currentImage.height)];
        [image drawInRect:dirtyRect fromRect:NSMakeRect(0, 0, currentImage.width, currentImage.height) operation:NSCompositeCopy fraction:1.0];
        [image release];
    }
}

- (void)dealloc
{
    if (imagePaths) {
        [imagePaths release];
        [tracker release];
        [gradImage release];
        [histEqImage release];
        [morpImage release];
        [scratchImage release];
    }
    free(cellDidDivide);
    free(kernel);
    [currentImage release];
    [super dealloc];
}

@end

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Michelle Gutwein, Kristin C. Gunsalus, and Davi Geiger.
//  Label free cell-tracking and division detection based on 2D time-lapse images for lineage analysis of early embryo development.
//  Computers in Biology and Medicine. Available online 9 May 2014.
//  http://www.sciencedirect.com/science/article/pii/S0010482514000973
