//
//  Tracker.h
//  CellTracker
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#import <Foundation/Foundation.h>
#import "OCVFloatImage.h"
#import "Cell.h"

@interface Tracker : NSObject {
    int nFrames;
    Cell *** cells;
    Cell ** c0Array;
    Cell ** c00Array;
    Cell ** c01Array;
    Cell ** c000Array;
    Cell ** c001Array;
    Cell ** c010Array;
    Cell ** c011Array;
    OCVFloatImage * bandImageG1;
    OCVFloatImage * bandImageG2;
    OCVFloatImage * bandImageG3;
    OCVFloatImage * dpBufferG1;
    OCVFloatImage * dpBufferG2;
    OCVFloatImage * dpBufferG3;
    OCVFloatImage * dpCostG1;
    OCVFloatImage * dpCostG2;
    OCVFloatImage * dpCostG3;
    OCVFloatImage * dpWeightsG1;
    OCVFloatImage * dpWeightsG2;
    OCVFloatImage * dpWeightsG3;
    OCVFloatImage * pathCostsG1;
    OCVFloatImage * pathCostsG2;
    OCVFloatImage * pathCostsG3;
    OCVFloatImage * predecessorsG1;
    OCVFloatImage * predecessorsG2;
    OCVFloatImage * predecessorsG3;
    int * dpSolutionG1;
    int * dpSolutionG2;
    int * dpSolutionG3;
}

@property(readwrite, assign) Cell *** cells;

- (id)initForNFrames:(int)theNFrames;
- (void)initCellArray:(Cell **)theCellArray forGeneration:(int)theGeneration;
- (void)freeCellArray:(Cell **)theCellArray;
- (void)initWeights:(OCVFloatImage *)theWeights;
- (void)trackWithGradientImage:(OCVFloatImage * )theGradientImage index:(int)theIndex;
- (void)fitBoundaryOfCell:(Cell *)theCell inImage:(OCVFloatImage *)theImage;
- (int)getBandImage:(OCVFloatImage *)theBandImage forCell:(Cell *)theCell fromImage:(OCVFloatImage *)inputImage;
- (void)dpSolveForBand:(OCVFloatImage *)theImageBand cellGeneration:(int)theGeneration;
- (void)computePathCostsAndPredecessorsForCellGeneration:(int)theGeneration;
- (void)dpOptimalPathForCellGeneration:(int)theGeneration;
- (int)radiusForGeneration:(int)theGeneration;
- (int)nLocationsForRadius:(int)theRadius;
- (int)bandLengthForRadius:(int)theRadius;

@end

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Michelle Gutwein, Kristin C. Gunsalus, and Davi Geiger.
//  Label free cell-tracking and division detection based on 2D time-lapse images for lineage analysis of early embryo development.
//  Computers in Biology and Medicine. Available online 9 May 2014.
//  http://www.sciencedirect.com/science/article/pii/S0010482514000973
