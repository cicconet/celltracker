//
//  main.m
//  CellTracker
//
//  Created by Marcelo Cicconet on 9/3/12.
//  Copyright (c) 2012 Marcelo Cicconet. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
