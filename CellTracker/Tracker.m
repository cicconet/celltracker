//
//  Tracker.m
//  CellTracker
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#import "Tracker.h"

@implementation Tracker

@synthesize cells;

- (id)initForNFrames:(int)theNFrames
{
    if (self = [super init]) {
        nFrames = theNFrames;
        
        int r = [self radiusForGeneration:1]; int nl = [self nLocationsForRadius:r]; int bl = [self bandLengthForRadius:r];
        bandImageG1    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpBufferG1     = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpCostG1       = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpWeightsG1    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        pathCostsG1    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        predecessorsG1 = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpSolutionG1 = (int *)malloc(nl*sizeof(int));
        r = [self radiusForGeneration:2]; nl = [self nLocationsForRadius:r]; bl = [self bandLengthForRadius:r];
        bandImageG2    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpBufferG2     = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpCostG2       = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpWeightsG2    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        pathCostsG2    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        predecessorsG2 = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpSolutionG2 = (int *)malloc(nl*sizeof(int));
        r = [self radiusForGeneration:3];  nl = [self nLocationsForRadius:r]; bl = [self bandLengthForRadius:r];
        bandImageG3    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpBufferG3     = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpCostG3       = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpWeightsG3    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        pathCostsG3    = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        predecessorsG3 = [[OCVFloatImage alloc] initWithData:NULL width:nl height:bl];
        dpSolutionG3 = (int *)malloc(nl*sizeof(int));
        
        [self initWeights:dpWeightsG1];
        [self initWeights:dpWeightsG2];
        [self initWeights:dpWeightsG3];
        
        cells = (Cell ***)malloc(7*sizeof(Cell **));
        
        cells[0] = (Cell **)malloc(theNFrames*sizeof(Cell *));
        cells[1] = (Cell **)malloc(theNFrames*sizeof(Cell *));
        cells[2] = (Cell **)malloc(theNFrames*sizeof(Cell *));
        cells[3] = (Cell **)malloc(theNFrames*sizeof(Cell *));
        cells[4] = (Cell **)malloc(theNFrames*sizeof(Cell *));
        cells[5] = (Cell **)malloc(theNFrames*sizeof(Cell *));
        cells[6] = (Cell **)malloc(theNFrames*sizeof(Cell *));
        
        [self initCellArray:cells[0] forGeneration:1];
        [self initCellArray:cells[1] forGeneration:2];
        [self initCellArray:cells[2] forGeneration:2];
        [self initCellArray:cells[3] forGeneration:3];
        [self initCellArray:cells[4] forGeneration:3];
        [self initCellArray:cells[5] forGeneration:3];
        [self initCellArray:cells[6] forGeneration:3];
        
        c0Array = cells[0];
        c00Array = cells[1];
        c01Array = cells[2];
        c000Array = cells[3];
        c001Array = cells[4];
        c010Array = cells[5];
        c011Array = cells[6];
    }
    return self;
}

- (void)initCellArray:(Cell **)theCellArray forGeneration:(int)theGeneration
{
    for (int i = 0; i < nFrames; i++) {
        int r = [self radiusForGeneration:theGeneration];
        theCellArray[i] = [[Cell alloc] initWithNLocations:[self nLocationsForRadius:r] bandLength:[self bandLengthForRadius:r]];
    }
}

- (void)freeCellArray:(Cell **)theCellArray
{
    for (int i = 0; i < nFrames; i++) {
        [theCellArray[i] release];
    }
}

- (void)initWeights:(OCVFloatImage *)theWeights
{
    int i0 = theWeights.height/2;
    float * w = (float *)calloc(theWeights.height, sizeof(float));
    for (int i = 0; i < theWeights.height; i++) {
        float d = fabsf(i-i0)/(float)i0;
        w[i] = 0.5*d*d;
    }
    for (int j = 0; j < theWeights.width; j++) {
        for (int i = 0; i < theWeights.height; i++) {
            theWeights.data[i*theWeights.width+j] = w[i];
        }
    }
    free(w);
}

- (void)trackWithGradientImage:(OCVFloatImage * )theGradientImage index:(int)theIndex
{
    for (int i = 0; i < 7; i++) {
        Cell * cell = cells[i][theIndex];
        if (cell.active) { // active, generation, centerRow, centerCol and radius variables should be initialized by View
            float rad = cell.radius;
            float estrad = [self radiusForGeneration:cell.generation];
            if (rad < 0.9*estrad) rad = 0.9*estrad;
            if (rad > 1.1*estrad) rad = 1.1*estrad;
            cell.radius = rad;
            [self fitBoundaryOfCell:cell inImage:theGradientImage];
            cell.tracked = YES;
        }
    }
}

- (void)fitBoundaryOfCell:(Cell *)theCell inImage:(OCVFloatImage *)theImage
{
    OCVFloatImage * bandImage = nil;
    OCVFloatImage * cost = nil;
    int * solution;
    switch (theCell.generation) {
        case 1: {
            bandImage = bandImageG1; cost = dpCostG1; solution = dpSolutionG1; break;
        }
        case 2: {
            bandImage = bandImageG2; cost = dpCostG2; solution = dpSolutionG2; break;
        }
        case 3: {
            bandImage = bandImageG3; cost = dpCostG3; solution = dpSolutionG3; break;
        }
        default:
            break;
    }
    int r0 = [self getBandImage:bandImage forCell:theCell fromImage:theImage];
    [bandImage normalize];
    [self dpSolveForBand:bandImage cellGeneration:theCell.generation];
    for (int i = 0; i < bandImage.width; i++) {
        solution[i] += r0;
    }
    
//    [cost normalize];
//    for (int j = 0; j < cost.width; j++) {
//        cost.data[solution[j]*cost.width+j] = 1.0;
//    }
//    [cost savePNGToFilePath:@"/Users/Cicconet/Desktop/Image.png"];

    point * boundary = theCell.boundary;
    int nPoints = cost.width;
    for (int i = 0; i < nPoints; i++) {
        point p;
        float alpha = (float)i/(float)nPoints*2.0*M_PI;
        p.row = theCell.centerRow+roundf(solution[i]*cosf(alpha));
        p.col = theCell.centerCol+roundf(solution[i]*sinf(alpha));
        if (p.row < 0) p.row = 0;
        if (p.row > theImage.height-1) p.row = theImage.height-1;
        if (p.col < 0) p.col = 0;
        if (p.col > theImage.width-1) p.col = theImage.width-1;
        boundary[i] = p;
    }
    float mr = 0.0;
    float mc = 0.0;
    for (int i = 0; i < nPoints; i++) {
        mr = mr+boundary[i].row;
        mc = mc+boundary[i].col;
    }
    mr = mr/(float)nPoints;
    mc = mc/(float)nPoints;
    theCell.centerRow = (int)mr;
    theCell.centerCol = (int)mc;
    float radius = 0.0;
    for (int i = 0; i < nPoints; i++) {
        float dx = (boundary[i].row-theCell.centerRow);
        float dy = (boundary[i].col-theCell.centerCol);
        radius = radius+sqrtf(dx*dx+dy*dy);
    }
    radius = radius/(float)nPoints;
    theCell.radius = (int)radius;
}

- (int)getBandImage:(OCVFloatImage *)theBandImage forCell:(Cell *)theCell fromImage:(OCVFloatImage *)inputImage
{
    int row = theCell.centerRow;
    int col = theCell.centerCol;
    int bandLength = theBandImage.height;
    int nLocations = theBandImage.width;
    int r0 = theCell.radius-roundf(bandLength/2.0);
    int * radii = (int *)malloc(bandLength*sizeof(int));
    for (int i = 0; i < bandLength; i++) {
        radii[i] = r0+i;
    }
    float * alphas = (float *)malloc(nLocations*sizeof(float));
    for (int i = 0; i < nLocations; i++) {
        alphas[i] = (float)i/(float)nLocations*2.0*M_PI;
    }
    for (int j = 0; j < nLocations; j++) {
        for (int i = 0; i < bandLength; i++) {
            int r = row+roundf(radii[i]*cosf(alphas[j]));
            int c = col+roundf(radii[i]*sinf(alphas[j]));
            if (r >= 0  && r < inputImage.height && c >= 0 && c < inputImage.width) {
                theBandImage.data[i*theBandImage.width+j] = inputImage.data[r*inputImage.width+c];
            }
        }
    }
    free(alphas);
    free(radii);
    return r0;
}

- (void)dpSolveForBand:(OCVFloatImage *)theImageBand cellGeneration:(int)theGeneration
{
    // theImageBand should be normalized
    OCVFloatImage * cost = nil;
    OCVFloatImage * weights = nil;
    switch (theGeneration) {
        case 1: {
            cost = dpCostG1; weights = dpWeightsG1;
            break;
        }
        case 2: {
            cost = dpCostG2; weights = dpWeightsG2;
            break;
        }
        case 3: {
            cost = dpCostG3; weights = dpWeightsG3;
            break;
        }
        default:
            break;
    }
    for (int i = 0; i < cost.height; i++) {
        for (int j = 0; j < cost.width; j++) {
            int index = i*cost.width+j;
            cost.data[index] = 0.5*(1.0-theImageBand.data[index])+weights.data[index];
        }
    }
    [self computePathCostsAndPredecessorsForCellGeneration:theGeneration];
}

- (void)computePathCostsAndPredecessorsForCellGeneration:(int)theGeneration
{
    OCVFloatImage * dpCost = nil;
    OCVFloatImage * pathCosts = nil;
    OCVFloatImage * predecessors = nil;
    switch (theGeneration) {
        case 1: {
            pathCosts = pathCostsG1; predecessors = predecessorsG1; dpCost = dpCostG1;
            break;
        }
        case 2: {
            pathCosts = pathCostsG2; predecessors = predecessorsG2; dpCost = dpCostG2;
            break;
        }
        case 3: {
            pathCosts = pathCostsG3; predecessors = predecessorsG3; dpCost = dpCostG3;
            break;
        }
        default:
            break;
    }
    int nRows = dpCost.height;
    int nCols = dpCost.width;
    for (int i = 0; i < nRows; i++) {
        pathCosts.data[i*nCols] = dpCost.data[i*nCols];
    }
    for (int j = 0; j < nCols; j++) {
        pathCosts.data[j] = INFINITY;
        pathCosts.data[nCols+j] = INFINITY;
        pathCosts.data[(nRows-1)*nCols+j] = INFINITY;
        pathCosts.data[(nRows-2)*nCols+j] = INFINITY;
    }
    for (int j = 1; j < nCols; j++) {
        for (int i = 2; i < nRows-2; i++) {
            float v5[2];
            float v3[3];
            v5[0] = pathCosts.data[(i-2)*nCols+j-1];
            v3[0] = pathCosts.data[(i-1)*nCols+j-1];
            v3[1] = pathCosts.data[i    *nCols+j-1];
            v3[2] = pathCosts.data[(i+1)*nCols+j-1];
            v5[1] = pathCosts.data[(i+2)*nCols+j-1];
            float mm = INFINITY;
            for (int k = 0; k < 3; k++) { if (v3[k] < mm) mm = v3[k]; }
            float m = mm;
            if (v5[0] < m) m = v5[0];
            if (v5[1] < m) m = v5[1];
            if (m < 0.99*mm) {
                pathCosts.data[i*nCols+j] = m+dpCost.data[i*nCols+j];
                if (m == pathCosts.data[(i-2)*nCols+j-1]) {
                    predecessors.data[i*nCols+j] = -2.0;
                } else if (m == pathCosts.data[(i-1)*nCols+j-1]) {
                    predecessors.data[i*nCols+j] = -1.0;
                } else if (m == pathCosts.data[i*nCols+j-1]) {
                    predecessors.data[i*nCols+j] = 0.0;
                } else if (m == pathCosts.data[(i+1)*nCols+j-1]) {
                    predecessors.data[i*nCols+j] = 1.0;
                } else {
                    predecessors.data[i*nCols+j] = 2.0;
                }
            } else {
                pathCosts.data[i*nCols+j] = mm+dpCost.data[i*nCols+j];
                if (mm == pathCosts.data[(i-1)*nCols+j-1]) {
                    predecessors.data[i*nCols+j] = -1.0;
                } else if (mm == pathCosts.data[i*nCols+j-1]) {
                    predecessors.data[i*nCols+j] = 0.0;
                } else {
                    predecessors.data[i*nCols+j] = 1.0;
                }
            }
        }
    }
    [self dpOptimalPathForCellGeneration:theGeneration];
}

- (void)dpOptimalPathForCellGeneration:(int)theGeneration
{
    int * dpSolution;
    OCVFloatImage * pathCosts = nil;
    OCVFloatImage * predecessors = nil;
    switch (theGeneration) {
        case 1: {
            dpSolution = dpSolutionG1; pathCosts = pathCostsG1; predecessors = predecessorsG1;
            break;
        }
        case 2: {
            dpSolution = dpSolutionG2; pathCosts = pathCostsG2; predecessors = predecessorsG2;
            break;
        }
        case 3: {
            dpSolution = dpSolutionG3; pathCosts = pathCostsG3; predecessors = predecessorsG3;
            break;
        }
        default:
            break;
    }
    int im = 0;
    float m = INFINITY;
    for (int i = 0; i < pathCosts.height; i++) {
        float v = pathCosts.data[i*pathCosts.width+(pathCosts.width-1)];
        if (v < m) {
            m = v;
            im = i;
        }
    }
    dpSolution[pathCosts.width-1] = im;
    for (int j = pathCosts.width-2; j >= 0; j--) {
        dpSolution[j] = dpSolution[j+1]+predecessors.data[(int)dpSolution[j+1]*pathCosts.width+(j+1)];
    }
}

- (int)radiusForGeneration:(int)theGeneration
{
    int rd = 120;
    if (theGeneration > 1) {
        for (int i = 1; i < theGeneration; i++) {
            float v = 4.0/3.0*M_PI*powf(rd, 3.0);
            v /= 2.0;
            rd = floorf(powf(v/4.0*3.0/M_PI,1.0/3.0));
        }
    }
    return rd;
}

- (int)nLocationsForRadius:(int)theRadius
{
    float circlelength = 2.0*M_PI*theRadius;
    return 10*floorf(circlelength/4.0/10.0);
}

- (int)bandLengthForRadius:(int)theRadius
{
    return 2*floorf(theRadius/2.0);
}

- (void)dealloc
{
    [bandImageG1 release];
    [bandImageG2 release];
    [bandImageG3 release];
    [dpBufferG1 release];
    [dpBufferG2 release];
    [dpBufferG3 release];
    [dpCostG1 release];
    [dpCostG2 release];
    [dpCostG3 release];
    [dpWeightsG1 release];
    [dpWeightsG2 release];
    [dpWeightsG3 release];
    [pathCostsG1 release];
    [pathCostsG2 release];
    [pathCostsG3 release];
    [predecessorsG1 release];
    [predecessorsG2 release];
    [predecessorsG3 release];
    free(dpSolutionG1);
    free(dpSolutionG2);
    free(dpSolutionG3);
    
    [self freeCellArray:c0Array];
    [self freeCellArray:c00Array];
    [self freeCellArray:c01Array];
    [self freeCellArray:c000Array];
    [self freeCellArray:c001Array];
    [self freeCellArray:c010Array];
    [self freeCellArray:c011Array];
    
    free(c0Array) ;
    free(c00Array);
    free(c01Array);
    free(c000Array);
    free(c001Array);
    free(c010Array);
    free(c011Array);
    
    free(cells);
    
    [super dealloc];
}

@end

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Michelle Gutwein, Kristin C. Gunsalus, and Davi Geiger.
//  Label free cell-tracking and division detection based on 2D time-lapse images for lineage analysis of early embryo development.
//  Computers in Biology and Medicine. Available online 9 May 2014.
//  http://www.sciencedirect.com/science/article/pii/S0010482514000973
