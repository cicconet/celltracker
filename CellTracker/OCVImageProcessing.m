//
//  OCVImageProcessing.m
//  CellTracker
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#import "OCVImageProcessing.h"

@implementation OCVImageProcessing

+ (void)gradient:(OCVFloatImage *)output input:(OCVFloatImage *)input
{
    int nRows = input.height;
    int nCols = input.width;
    for (int i = 1; i < nRows; i++) {
        for (int j = 1; j < nCols; j++) {
            float dx = input.data[i*nCols+j]-input.data[(i-1)*nCols+j];
            float dy = input.data[i*nCols+j]-input.data[i*nCols+(j-1)];
            output.data[i*nCols+j] = sqrtf(dx*dx+dy*dy);
        }
    }
    for (int i = 1; i < nRows; i++) {
        output.data[i*nCols] = output.data[i*nCols+1];
    }
    for (int j = 0; j < nCols; j++) {
        output.data[j] = output.data[nCols+j];
    }
}

// ----------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Histogram Operations
// ----------------------------------------------------------------------------------------------------

+ (void)histogram:(int *)histogram input:(OCVFloatImage *)image nBins:(int)nBins
{
    vImage_Buffer imIn = [image vImageBufferStructure];
    vImagePixelCount * pixelCount = (vImagePixelCount *)malloc(nBins*sizeof(vImagePixelCount));
    float min, max;
    [image getRangeOutMin:&min outMax:&max];
    vImageHistogramCalculation_PlanarF(&imIn, pixelCount, nBins, min, max, kvImageNoFlags);
    for (int i = 0; i < nBins; i++) {
        histogram[i] = (int)pixelCount[i];
    }
    free(pixelCount);
}

+ (void)histogramEqualization:(OCVFloatImage *)output input:(OCVFloatImage *)input;
{
    vImage_Buffer imIn = [input vImageBufferStructure];
    vImage_Buffer imOut = [output vImageBufferStructure];
    float min, max;
    [input getRangeOutMin:&min outMax:&max];
    vImageEqualization_PlanarF(&imIn, &imOut, NULL, 256, min, max, kvImageNoFlags);
}

+ (void)adaptiveHistogramEqualization:(OCVFloatImage *)output
                                input:(OCVFloatImage *)input
                           nBlockRows:(int)nBlockRows
                           nBlockCols:(int)nBlockCols
{
    int nbRows = nBlockRows;
    int nbCols = nBlockCols;
    
    int rowStep = input.height/nbRows;
    int colStep = input.width/nbCols;
    
    int * histogram = (int *)malloc(256*sizeof(int));
    int * chistogram = (int *)malloc(256*sizeof(int));
    int * blockRowCenters = (int *)malloc(nbRows*sizeof(int));
    int * blockColCenters = (int *)malloc(nbCols*sizeof(int));
    float ** mappings = (float **)malloc(nbRows*nbCols*sizeof(float *));
    for (int i = 0; i < nbRows*nbCols; i++) {
        mappings[i] = (float *)malloc(256*sizeof(float));
    }
    
    for (int i = 0; i < nbRows; i++) {
        for (int j = 0; j < nbCols; j++) {
            OCVFloatImage * subImage = [[OCVFloatImage alloc] initWithData:NULL width:colStep height:rowStep];
            
            int ii0 = i*rowStep;
            int jj0 = j*colStep;
            
            for (int ii = 0; ii < rowStep; ii++) {
                for (int jj = 0; jj < colStep; jj++) {
                    subImage.data[ii*colStep+jj] = input.data[(ii0+ii)*input.width+(jj0+jj)];
                }
            }
            
            [OCVImageProcessing histogram:histogram input:subImage nBins:256];

            chistogram[0] = histogram[0];
            for (int k = 1; k < 256; k++) {
                chistogram[k] = chistogram[k-1]+histogram[k];
            }

            int n = chistogram[255];
            int p0 = n/200; // 0.5%
            int p1 = chistogram[255]-p0;
            int k0 = 0, k1 = 255;
            for (int k = 0; k < 255; k++) {
                if (chistogram[k] > p0) {
                    k0 = k;
                    break;
                }
            }
            for (int k = 255; k >= 0; k--) {
                if (chistogram[k] < p1) {
                    k1 = k;
                    break;
                }
            }

            int index = i*nbCols+j;
            for (int k = 0; k < 256; k++) {
                if (k < k0) {
                    mappings[index][k] = 0.0;
                } else if (k > k1) {
                    mappings[index][k] = 1.0;
                } else {
                    mappings[index][k] = (float)(k-k0)/(float)(k1-k0);
                }
            }
            blockRowCenters[i] = ii0+rowStep/2;
            blockColCenters[j] = jj0+colStep/2;
            
            [subImage release];
        }
    }
    
    float * weights = (float *)malloc(nbRows*nbCols*sizeof(float));
    float f = 1.0;
    float stdev = f*(float)rowStep;
    if (colStep > rowStep) {
        stdev = f*(float)colStep;
    }
    for (int i = 0; i < input.height; i++) {
        for (int j = 0; j < input.width; j++) {
            float s = 0.0;
            for (int ii = 0; ii < nbRows; ii++) {
                for (int jj = 0; jj < nbCols; jj++) {
                    float d2 = powf(i-blockRowCenters[ii], 2.0)+powf(j-blockColCenters[jj], 2.0);
                    weights[ii*nbCols+jj] = expf(-0.5*d2/(stdev*stdev));
                    s += weights[ii*nbCols+jj];
                }
            }
            for (int k = 0; k < nbRows*nbCols; k++) {
                weights[k] = weights[k]/s;
            }
            float m = 0.0;
            for (int k = 0; k < nbRows*nbCols; k++) {
                m += weights[k]*mappings[k][(int)(input.data[i*input.width+j]*255.0)];
            }
            output.data[i*output.width+j] = m;
        }
    }
    free(weights);
    
    free(blockRowCenters);
    free(blockColCenters);
    for (int i = 0; i < nbRows*nbCols; i++) {
        free(mappings[i]);
    }
    free(histogram);
    free(chistogram);
    free(mappings);
}

// ----------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Morphological Operations
// ----------------------------------------------------------------------------------------------------

+ (OCVMorphologicalKernel *)allocMorphologicalKernelWithSize:(int)theSize
{
    OCVMorphologicalKernel * kernel = (OCVMorphologicalKernel *)malloc(sizeof(OCVMorphologicalKernel));
    kernel->size = theSize;
    kernel->data = (float *)malloc(theSize*theSize*sizeof(float));
    for (int i = 0; i < theSize*theSize; i++) {
        kernel->data[i] = 1.0;
    }
    return kernel;
}

+ (void)releaseMorphologicalKernel:(OCVMorphologicalKernel *)kernel
{
    free(kernel->data);
    free(kernel);
}

+ (void)erosion:(OCVFloatImage *)output input:(OCVFloatImage *)input kernel:(float *)kernel kernelSize:(int)kernelSize
{
    vImage_Buffer imIn = [input vImageBufferStructure];
    vImage_Buffer imOut = [output vImageBufferStructure];
    vImageErode_PlanarF(&imIn, &imOut, 0, 0, kernel, kernelSize, kernelSize, kvImageNoFlags);
}

+ (void)dilatation:(OCVFloatImage *)output input:(OCVFloatImage *)input kernel:(float *)kernel kernelSize:(int)kernelSize
{
    vImage_Buffer imIn = [input vImageBufferStructure];
    vImage_Buffer imOut = [output vImageBufferStructure];
    vImageDilate_PlanarF(&imIn, &imOut, 0, 0, kernel, kernelSize, kernelSize, kvImageNoFlags);
}

@end

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Michelle Gutwein, Kristin C. Gunsalus, and Davi Geiger.
//  Label free cell-tracking and division detection based on 2D time-lapse images for lineage analysis of early embryo development.
//  Computers in Biology and Medicine. Available online 9 May 2014.
//  http://www.sciencedirect.com/science/article/pii/S0010482514000973
